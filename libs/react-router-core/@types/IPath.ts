export interface PathProps { [key:string]: string }

export default interface IPath <TParams extends PathProps = any> {
    /** URI. */
    path: string;
    /** Params to be replaced in the URI. */
    params?: TParams;
}