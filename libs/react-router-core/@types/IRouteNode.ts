/**
 * Route Node.
 * 
 * A react-router Route Node.
 */
export default interface IRouteNode {
    /** Component to render on screen when the URI matches. */
    component: React.ReactNode;
    /** URI to match. If no path is given then it will always be a match. */
    path?: string;
    /** If true the URI must be exact in order to render the component. If false
     * then the component will render on partial match of the URI.
     */
    exact?: boolean;
    /**
     * List of children to render.
     */
    routes?: IRouteNode[];
    /** Redirect to the following area if unauthorised. */
    redirect?: string;
}