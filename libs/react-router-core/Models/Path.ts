import { PathProps } from "../@types/IPath";

export default class Path <T extends PathProps = any> {
    constructor (path?: string, params?: T) {
        this._path  = path;
        this._params = params;
    }

    private _path: string | undefined;
    private _params: T | undefined;

    public get path () {
        return this._path;
    }

    public get params () {
        return this._params;
    }
}