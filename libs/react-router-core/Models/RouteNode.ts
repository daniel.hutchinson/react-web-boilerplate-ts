import { IRouteNode } from "../@types";

export default class RouteNode implements IRouteNode {
    constructor (
        component: React.ReactNode, 
        path?: string, 
        exact?: boolean, 
        routes?: IRouteNode[], 
        redirect?: string) {
            this._component = component;
            this._path = path;
            this._exact = exact;
            this._routes = routes;
            this._redirect = redirect;
    }

    private _component: any;
    private _path: string | undefined;
    private _exact: boolean | undefined;
    private _routes: IRouteNode[] | undefined;
    private _redirect: string | undefined;

    public get component () {
        return this._component;
    }

    public get path () {
        return this._path;
    }

    public get exact () {
        return this._exact;
    }

    public get routes () {
        return this._routes;
    }

    public get redirect () {
        return this._redirect;
    }
}