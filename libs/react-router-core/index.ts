export * from './@types';
export * from './components';
export * from './methods';
export * from './Models';

import RoutesBase from './RoutesBase';
export default RoutesBase;