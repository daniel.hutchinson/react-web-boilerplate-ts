import PathFactory from "./factories/PathFactory";
import RouteNodeFactory from "./factories/RouteNodeFactory";
import { IRouteNode } from ".";

interface CreateModel {
    component: React.ReactNode;
    path?: string;
    exact?: boolean;
    children?: IRouteNode[];
    redirect?: string;
}

export default class RoutesBase {
    constructor () {
        this.pathFactory = new PathFactory();
        this.routeFactory = new RouteNodeFactory();
    }

    private pathFactory: PathFactory;
    private routeFactory: RouteNodeFactory;

    public getRoutes () {
        const routes: any[] = [];

        const ignoredProperties = p => 
            p !== 'pathFactory' && p !== 'routeFactory' && p !== 'createRoute' && p !== 'createAuthorisedRoute' && p !== 'createRouteByModel' && p !== 'getRoutes';

        Object.keys(this).filter(ignoredProperties).map(p => {
            const node: IRouteNode = this[p]();

            routes.push({
                path: node.path,
                exact: node.exact,
                component: node.component,
                routes: node.routes,
                redirect: node.redirect
            });
        })

        return routes;
    }

    protected createRoute (
        component: React.ReactNode,
        path?: string,
        exact: boolean = true,
        children?: IRouteNode[],
        redirect?: string
    ) {
        // Get all properties of itself and then generate an object structure that 
        // allows routes.
        return this.routeFactory.create(
            component,
            path,
            exact,
            children,
            redirect
        )
    }

    protected createAuthorisedRoute ({
        component,
        path,
        exact,
        children,
        redirect = '/login'
    }: CreateModel) {
        return this.createRoute(
            component,
            path,
            exact,
            children,
            redirect
        );
    }

    protected createRouteByModel ({
        component,
        path,
        exact = true,
        children,
        redirect}: CreateModel) {
        return this.createRoute(
            component,
            path,
            exact,
            children,
            redirect
        );
    }
}