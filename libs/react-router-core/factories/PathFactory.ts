import IPath, { PathProps } from "../@types/IPath";
import { Path } from "..";

export default class PathFactory {
    constructor () {}

    public create = <TData extends PathProps> (path: string, data?: TData) => {
        return new Path<TData>(path, data);
    } 
}