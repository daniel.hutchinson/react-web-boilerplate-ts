import { IRouteNode } from "../@types";
import { RouteNode } from "../Models";

export default class RouteNodeFactory {
    constructor () {}

    public create = (
        component: React.ReactNode, 
        path?: string, 
        exact?: boolean, 
        children?: IRouteNode[],
        redirect?: string) => {
            return new RouteNode(component, path, exact, children, redirect);
        }
}