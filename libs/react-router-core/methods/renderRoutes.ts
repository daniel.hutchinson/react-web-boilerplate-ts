import { createElement } from 'react';
import { IRouteNode } from '../@types';

const renderRoutes = (routes: IRouteNode[], Component) => {
    const createComponent = (route, key, subPath = '') => {
        return createElement(
            Component,
            {
                key: key,
                ...route,
                path: `${subPath}${route.path}`
            }
        )
    }

    const getRoute = (route, key, subPath?) => {
        if (subPath) {
            return createComponent(route, key, subPath);
        }

        return createComponent(route, key);
    }

    const getRoutes = (routes: IRouteNode[], subPath?) => {
        let vals: any[] = [];
        routes.forEach((route: IRouteNode, i) => {
            const childRoutes = route.routes;
            if (childRoutes) {
                vals = [...vals, ...getRoutes(childRoutes, route.path)];
            }
            
            vals.push(getRoute(route, `${i}${route.path}`, subPath));
        });

        return vals;
    }

    return getRoutes(routes);
}

export default renderRoutes;