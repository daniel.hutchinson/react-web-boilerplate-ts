import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useWhyDidYouUpdate } from 'components';

export interface PrivateRoute {
    isAuthenticated: boolean;
    redirect?: string; // TODO: Mabye change this to an redirectTo object where the default is null. Means we don't need to check if this is here...
    redirectToState?: object; // This also wouldn't need to be passed in...
}

const PrivateRoute = ({ Component, component, path, isAuthenticated, redirect, redirectToState, ...rest }) => {
    useWhyDidYouUpdate('PrivateRoute', { Component, component, path, isAuthenticated, redirect, redirectToState, ...rest });

    const createRoute = (props) => React.createElement(component, props);
    
    const renderRoute = (props) => {
        if (!path) {
            return createRoute(props);
        }

        const isEitherLoginOrRegister = path.toLowerCase() === '/login' || path.toLowerCase() === '/register';
        if (isEitherLoginOrRegister) {
            if (isAuthenticated) {
                return <Redirect
                    to={{
                        pathname: redirect,
                        state: redirectToState
                    }}
                />
            }
            else {
                return React.createElement(component, props);
            }
        }

        if (isAuthenticated === false) {
            return <Redirect
                to={{
                    pathname: redirect,
                    state: redirectToState
                }}
            />
        }

        if (isAuthenticated && isEitherLoginOrRegister === false) {
                return React.createElement(component, props);
        }
        else if (redirect) {
            return <Redirect
                to={{
                    pathname: redirect,
                    state: redirectToState
                }}
            />
        }

        return null;
    }

    return (
        <Route 
            {...rest}
            Component={null}
            component={null}
            render={renderRoute}
        />
    );
}

// Render either:
  // - Login
  // - Component (if authorised)

// If Not authorised:
  // Render Login

export default PrivateRoute;