import * as React from 'react';

export type ValuesFromPrivateRouteProvider = {
    isAuthenticated: boolean;
    logout: () => void;
    login: () => void;
}

export interface PrivateRouteProviderProps {
    value: ValuesFromPrivateRouteProvider;
}

export const PrivateRouteContext = React.createContext<ValuesFromPrivateRouteProvider>({
    isAuthenticated: false,
    logout: () => null,
    login: () => null
});

const PrivateRouteProvider: React.FunctionComponent<PrivateRouteProviderProps> = props => {
    const value = {
        isAuthenticated: props.value.isAuthenticated,
        logout: props.value.logout,
        login: props.value.login
    }

    return (
        <PrivateRouteContext.Provider value={value}>
            { props.children }
        </PrivateRouteContext.Provider>
    );
};

export default PrivateRouteProvider;