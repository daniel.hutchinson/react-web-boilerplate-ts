import * as React from 'react';
import { PrivateRouteContext } from './PrivateRouteProvider';
import PrivateRoute from './PrivateRoute';

export interface PrivateRouteConnectProps {
    Route: any;
}

const PrivateRouteConnect: React.FunctionComponent<PrivateRouteConnectProps> = ({ Route, ...props }) => {
    const value = React.useContext<any>(PrivateRouteContext);

    return (
        <Route {...props} {...value} />
    );
}

PrivateRouteConnect.defaultProps = {
    Route: PrivateRoute
};

export default PrivateRouteConnect;