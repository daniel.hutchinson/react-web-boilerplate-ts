import * as React from 'react';
import { Card, Typography } from '@material-ui/core';

export interface ErrorComponentProps {
    errorInfo?: string;
}

const ErrorComponent: React.FunctionComponent<ErrorComponentProps> = props => {
    const { errorInfo } = props;
    return (
        <Card style={{ textAlign: 'center' }}>
            <Typography variant='h2'>Oops!</Typography>
            <Typography variant='display1'>Something appears to have gone wrong. Please contact for help.</Typography>
            <Typography variant='display1'>{errorInfo}</Typography>
        </Card>
    );
}

export default ErrorComponent;