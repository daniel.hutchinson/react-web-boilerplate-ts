import * as React from 'react';
import ErrorComponent from './ErrorComponent';

interface ErrorBoundaryState {
    hasError: boolean;
}

export interface ErrorBoundaryProps {
    ErrorComponent?: any;
}

class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
    state = { hasError: false };

    static defaultProps = {
        ErrorComponent: ErrorComponent
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    componentDidCatch (error, info) {
    }

    render () {
        if (this.state.hasError) {
            const { ErrorComponent } = this.props;
            return <ErrorComponent />
        }

        return this.props.children;
    }
}

export default ErrorBoundary;