import { useRef, useEffect } from 'react';

const useWhyDidYouUpdate = (name, props) => {
    // Get comparison for next time hook runs.
    const previousProps = useRef<any>(null);

    useEffect(() => {
        if (previousProps) {
            if (previousProps.current) {
                // Get all of the keys from previous and current props.
                const allKeys = Object.keys({ ...previousProps, ...props });

                const changesObj = {};

                allKeys.forEach(key => {
                    const isDifferentFromCurrentProp = previousProps.current[key] !== props[key];
                    if (isDifferentFromCurrentProp) {
                        changesObj[key] = {
                            from: previousProps.current[key],
                            to: props[key]
                        }
                    }

                    if (Object.keys(changesObj).length) {
                        console.log('[why-did-you-update]', name, changesObj);
                    }
                
                    previousProps.current = props;
                })
            }
        }
    });
}

export default useWhyDidYouUpdate;