import * as React from 'react';
import withContainer from 'HOCs/withContainer';

export interface AuthorisedRoutesProps {
    isUserAuthorised?: boolean;
    isUserLoggedIn?: boolean;
}

class AuthorisedRoutesContainer extends React.Component<{Component: any}> {
    render () {
    const { Component } = this.props;
    return (
            <Component {...this.props} />
        );
    }
}

// check if user is logged in, if not, redirect
const AuthorisedRoutes = withContainer(AuthorisedRoutesContainer)(props => (
    <>
        { 
            props.isUserAuthorised && props.isUserLoggedIn
                && props.children
                || null 
        }
    </>
));

export default AuthorisedRoutes;