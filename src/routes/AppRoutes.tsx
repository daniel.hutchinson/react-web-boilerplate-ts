import RoutesBase from 'react-router-core';
import Login from 'components/pages/Login';
import Dashboard from 'components/pages/Dashboard';
import Register from 'components/pages/Register';
import MasterLayout from 'components/layouts/MasterLayout';

export class AppRoutes extends RoutesBase {
    protected Login = () => super.createRouteByModel({component: Login, path: "/login", redirect: '/'});
    protected Register = () => super.createRouteByModel({component: Register, path: "/register", redirect: '/'});

    // protected Master = () => super.createRouteByModel({component: MasterLayout, children: [
        // super.createAuthorisedRoute({component: Dashboard, path: "/"})
    // ]})
    protected Dashboard = () => super.createAuthorisedRoute({component: Dashboard, path: "/"});
}

const appRoutes = new AppRoutes();
export default appRoutes;