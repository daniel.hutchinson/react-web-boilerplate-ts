import * as React from 'react';
import HeaderBar from 'components/organisms/HeaderBar/HeaderBar';

const MasterLayout = props => {
    return ( 
        <>
            <HeaderBar  />
            {
                props.children
            }
        </>
    );
}

export default MasterLayout;