import * as React from 'react';
import { AppBar, Toolbar, Button } from '@material-ui/core'
import withContainer from 'HOCs/withContainer';
import HeaderBarContainer, { PropsFromHeaderBarContianer } from './containers/HeaderBarContainer';

export interface HeaderBarProps extends PropsFromHeaderBarContianer {
}

const HeaderBar = withContainer(HeaderBarContainer)(({ isUserLoggedIn, handleLogin, handleLogout}) => {
    return (
        <AppBar position='static'>
            <Toolbar>
                {
                    isUserLoggedIn 
                    ? null
                    // ? (
                    //     <Button onClick={handleLogin}>
                    //         Login
                    //     </Button>
                    // ) 
                    : (
                        <Button onClick={handleLogout}>
                            Log out
                        </Button>
                    )
                }
            </Toolbar>
        </AppBar>
    );
});

export default HeaderBar;