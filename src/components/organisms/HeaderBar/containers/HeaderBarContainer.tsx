import * as React from 'react';
import { AuthenticatedSessionContext } from 'components/App';
import { PrivateRouteContext } from 'react-router-core';

export interface HeaderBarContainerProps {
    Component: React.ComponentType<any>;
    children: any;
}

export interface PropsFromHeaderBarContianer {
    isUserLoggedIn: boolean;
    handleLogin: () => void;
    handleLogout: () => void;
}

const HeaderBarContainer: React.FunctionComponent<HeaderBarContainerProps> = ({ Component }) => 
{    
    const { loggedIn, setLoggedIn } = React.useContext(AuthenticatedSessionContext);
    const { logout } = React.useContext(PrivateRouteContext);
    const handleLogout = (setLoggedIn) => () => {
        logout();
        // setLoggedIn(false);
    }

    const handleLogin = (setLoggedIn) => () => {
        setLoggedIn(true);
    }

    return <Component isUserLoggedIn={loggedIn} handleLogin={handleLogin(setLoggedIn)} handleLogout={handleLogout(setLoggedIn)} />;
}

export default HeaderBarContainer;