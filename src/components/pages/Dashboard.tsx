import * as React from 'react';
import { PrivateRouteContext } from 'react-router-core';
import { Button } from '@material-ui/core';
import HeaderBar from 'components/organisms/HeaderBar/HeaderBar';

const Dashboard = () => {
    const { logout } = React.useContext(PrivateRouteContext);
    
    return (
        <HeaderBar />
    );
}

export default Dashboard;