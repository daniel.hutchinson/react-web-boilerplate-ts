import * as React from 'react'
import { Button } from '@material-ui/core';
import { PrivateRouteContext } from 'react-router-core';

export interface LoginProps {
    
}

const Login: React.FunctionComponent<LoginProps> = props => {
    const { login } = React.useContext(PrivateRouteContext);
    
    return (
        <Button onClick={login}>Login</Button>
    );
}

export default Login;