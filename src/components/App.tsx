import * as React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import appRoutes from 'routes/AppRoutes';
import { renderRoutes, PrivateRouteProvider } from 'react-router-core';
import PrivateRouteConnect from 'react-router-core/components/PrivateRouteConnect';
import { ErrorBoundary } from 'components';

export const AuthenticatedSessionContext = React.createContext<any>(null);

const AuthenticatedSessionProvider = props => {
    const [ loggedIn, setLoggedIn ] = React.useState(false);

    const value = {
        loggedIn,
        setLoggedIn
    }

    return (
        <AuthenticatedSessionContext.Provider value={value}>
            {
                props.children
            }
        </AuthenticatedSessionContext.Provider>
    )
}

export const App: React.SFC = props  => {
    const [isAuthenticated, set] = React.useState(false);

    const handleSet = () => {
        set(!isAuthenticated);
    }

    return (
        <ErrorBoundary>
            <AuthenticatedSessionProvider>
                <PrivateRouteProvider value={{ isAuthenticated: isAuthenticated, logout: handleSet, login: handleSet }}>
                    <BrowserRouter>
                        <Switch>
                            { renderRoutes(appRoutes.getRoutes(), PrivateRouteConnect) }
                        </Switch>
                    </BrowserRouter>
                </PrivateRouteProvider>
            </AuthenticatedSessionProvider>
        </ErrorBoundary>
    );
}