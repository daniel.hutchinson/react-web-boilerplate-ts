import * as React from 'react';

export interface withContainerProps <TComponentProps = any> {
    Component: React.ReactElement<TComponentProps>;
}

// function withContainer<TContainerProps extends withContainerProps = any, TWrappedComponentProps = any, TExclude = any> (Container: any) {
//     return function (wrappedComponent: React.ReactType<TWrappedComponentProps>): React.ComponentType<TContainerProps & Exclude<TExclude, TWrappedComponentProps>> {
//         return class extends React.Component<TContainerProps & Exclude<TExclude, TWrappedComponentProps>> {
//             render () {
//                 return React.createElement(
//                     Container,
//                     {
//                         ...this.props,
//                         Component: wrappedComponent
//                     }
//                 )
//             }
//         }
//     }
// }

function withContainer(Container) {
    return function (wrappedComponent) {
        return class extends React.Component {
            render () {
                return React.createElement(
                    Container,
                    {
                        ...this.props,
                        Component: wrappedComponent
                    }
                )
            }
        }
    }
}

// const withContainer = (Container: React.ReactElement<>) =>  (wrappedComponent): any =>  {
//     return class extends React.Component {
//         render () {
//             return <Container {...this.props} Component={wrappedComponent} />
//         }
//     }
// }

export default withContainer;