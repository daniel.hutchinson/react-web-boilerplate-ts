const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Stylish = require('webpack-stylish')

const paths = {
    OUT: path.resolve(__dirname, './public'),
    SRC: path.resolve(__dirname, './src'),
    LIB: path.resolve(__dirname, './libs')
};

module.exports = {
    context: __dirname,
    entry: {
        app: path.join(paths.SRC, 'index.tsx')
    },
    output: {
        filename: '[name].bundle.js',
        path: paths.OUT,
        publicPath: '/'
    },

    stats: 'none',

    resolve: {
        modules: [
            'node_modules',
            paths.LIB,
            paths.SRC
        ],

        extensions: [ '.ts', '.tsx', '.js' ]
    },

    // optimization: {
    //     splitChunks: {
    //         cacheGroups: {
    //             styles: {
    //                 name: 'styles',
    //                 test: /\.(scss|css)$/,
    //                 chunks: 'all',
    //                 enforce: true
    //             }
    //         }
    //     }
    // },

    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].bundle.css",
            chunkFileName: '[id].[name].bundle.css'
          }),
        new HtmlWebpackPlugin({ template: path.join(paths.SRC, 'index.html') }),
        new Stylish()
    ],  

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    { loader: 'babel-loader' },
                    { loader: 'awesome-typescript-loader' }
                ]
            }
        ]
    }
};