const base = require('./webpack.config.base');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = merge(base, {
    mode: 'development',

    devtool: 'source-map',

    devServer: {
        allowedHosts: [
            // Add your hosts here (api URLs)...
        ],
        port: 8080,
        open: 'Chrome',
        overlay: true,
        historyApiFallback: true
    },

    module: {
        rules: [
            {
                test:/\.s?css$/, 
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ],
            }
        ]
    }
});